package org.springframework.beans;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.AbstractBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class XmlBeanFactoryTest {
	public static void main(String[] args) {
		// 最基本的容器功能 通过xml 配置
		XmlBeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("bean.xml"));
		AbstractBeanFactory tempFactory = beanFactory;
		tempFactory.addBeanPostProcessor(null);  // 可以通过此方法向容器注入自己实现的BeanPostProcessor，达到干预 getBean()的目的

		Object object = beanFactory.getBean("beanName");

		// 容器功能的拓展 -- 支持注解方式配置
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
		applicationContext.addBeanFactoryPostProcessor(null); // 注入自定义的BeanFactoryPostProcessor类型后置处理器
		ApplicationContext temp = applicationContext;
		Object bean = temp.getBean("beanName");
	}
}
