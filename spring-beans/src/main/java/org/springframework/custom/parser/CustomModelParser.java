package org.springframework.custom.parser;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.custom.bean.CustomModelBean;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;
/**
 * 继承抽象类 AbstractSingleBeanDefinitionParser 实现自定义标签的解析逻辑
 * - 它会负责将xml 配置的标签解析成 BeanDefinition的形式，最终注册到 BeanFactory [ > BeanDefinitionRegistry]
 */
public class CustomModelParser extends AbstractSingleBeanDefinitionParser {
	@Override
	protected Class<?> getBeanClass(Element element) {
		return CustomModelBean.class;
	}
	@Override
	protected void doParse(Element element, BeanDefinitionBuilder builder) {
		String modelName = element.getAttribute("modelName");
		String desc = element.getAttribute("desc");
		String action = element.getAttribute("action");
		// 将从自定义标签提取到的属性注册到：BeanDefinitionBuilder；最终会注册到 BeanFactory中
		if (StringUtils.hasText(modelName)) {
			builder.addPropertyValue("modelName", modelName);
		}
		if (StringUtils.hasText(desc)) {
			builder.addPropertyValue("desc", desc);
		}
		if (StringUtils.hasText(action)) {
			builder.addPropertyValue("action", action);
		}
	}
}
