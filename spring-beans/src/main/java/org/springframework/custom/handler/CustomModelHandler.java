package org.springframework.custom.handler;
import org.springframework.beans.factory.xml.NamespaceHandlerSupport;
import org.springframework.custom.parser.CustomModelParser;
public class CustomModelHandler extends NamespaceHandlerSupport {
	@Override
	public void init() {
		/**
		 * 注册自定义标签的解析器到 BeanFactory 中
		 */
		registerBeanDefinitionParser("custom-model", new CustomModelParser());
	}
}
