# 1. FactoryBean 的使用  
bean 比较复杂的时候，需要大量配置信息。定制 bean 的实例化，从而摆脱大量配置信息，灵活性提高。如果： class="xx.xxx.FactoryBeanImpl"，getBean(beanName)，返回 FactoryBeanImpl.getObject();

# 2. 缓存中获取单例 bean；
单例 bean 只会被创建一次，优先从缓存中获取，获取失败，再进入加载流程。（确保容器中只有一个 bean）

# 3. 从 bean 实例获取对象，
 检查 bean 是否为 FactoryBean 类型。

# 4. 获取单例 bean (从缓存加载失败)；
从 ObjectFactory 获取 bean (回调它的 getObject() 方法，再去调用 creatBean()方法，创建 bean 由子类：spring-beans\src\main\java\org\springframework\beans\factory\support\AbstractAutowireCapableBeanFactory.java提供实现)
	``` java
        sharedInstance = getSingleton(beanName, new ObjectFactory<Object>() {
        		public Object getObject() throws BeansException{
       			// 准备完成后回调  由子类 AbstractAutowireCapableBeanFactory 实现方法
        			return createBean(beanName, mbd, args); 
        		}
        	}
    ```

# 5. 创建 bean  (createBean) 
* a.处理 overrride 属性 lookup-method （策略模式应用，bean替换？？） replace-method （动态替换方法的实现）
* b.实例化前  <后处理器>,
    对 BeanDefinition 的属性做前置化处理，提高拓展能力；  
    提供了一个短路逻辑，如果该 <后处理器> 返回值不为空，则直接调用 实例化后 的 <后处理器>，
    并把 通用的 bean 创建环节短路掉。
* c.实例化后  <后处理器>

# 6. 循环依赖
* a. 构造器循环依赖  无法解决
* b. setter 注入循环依赖   (bean 未创建完毕就将其 ObjectFactory 暴露，当别的bean 依赖他时，
即使该 bean 尚未完成加载，也可以从缓存中，获取到该提前暴露的 ObjectFactory )
* c. prototype(原型模式)范围的依赖处理 
该模式 bean spring 容器不会进行缓存，因此无法提前暴露一个 bean 

# 7. 创建 bean 
* a. 单例 bean 先清除缓存，确保 spring 容器中只有一个bean 
* b. 获取 bean 实例
    
    1. 如果指定了 supplier 属性：那么不通过反射去实例化bean， 而是通过回调 得到对象实例并返回。（面向函数式编程提供回调）
    
    2. 根据 BeanDefinition 判断是否存在工厂方法，如果存在，调用工厂方法并返回。
    
    3. 解析构造函数，多个构造函数是，根据 getBean() 入参个数匹配构造函数，否则使用默认构造函数实例化 bean;
    
* c. 依赖处理  

    根据 bean 是否单例，以及 xml 配置决定是否提前暴露对象  
    ```text
                    单例  && 允许循环依赖   && bean 正在创建   === 是否允许提早曝光
    		boolean earlySingletonExposure = (mbd.isSingleton() && this.allowCircularReferences &&
    				isSingletonCurrentlyInCreation(beanName));
    ```
    
* e. 属性填充  
    
    1. 给 InstantiationAwareBeanPostProcessors.postProcessAfterInstantiation  最后一次机会，
    通过属性注入改变 bean，它可以控制是否继续对bean进行属性填充。
    
    2. 根据类型（type）或者 名称（name）注入依赖的bean;
    
    3. InstantiationAwareBeanPostProcessor.postProcessPropertyValues 对需要依赖检查 的属性,在属性填充前，
     进行后处理器 处理；
     
    4. 将 PropertyValues [它缓存解析到的当前加载bean依赖的所有bean]  注入到 BeanWrapper[bean的包装器] 中。 

* f. 循环依赖检查

    spring 解决依赖循环只对单例有效，prototype作用范围的bean， spring没有较好的解决办法，只能抛出异常。

* g. 注册disposableBean 

    destory-method 定义的方法，对象销毁时调用。






