1. InitializingBean接口，保证初始化调用afterPropertiesSet

2. DisposableBean接口，保证销毁时调用destroy方法

3. BeanClassLoaderAware接口，保证初始化时调用setBeanClassLoader方法

4. FactoryBean接口，确保获取bean的时候不是直接获取该对象，而是调用其getObject()方法 

5. BeanNameAware接口， setBeanName() 方法 

6. BeanPostProcessor接口，实例化的时候调用：postProcessBeforeInitialization() 和 postProcessAfterInitialization()

7. MethodInterceptor 接口， 用于代理，实现了该接口的类都需要实现方法 invoke() 