/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.support;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.OrderComparator;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.lang.Nullable;

/**
 * Delegate for AbstractApplicationContext's post-processor handling.
 *
 * @author Juergen Hoeller
 * @since 4.0
 */
final class PostProcessorRegistrationDelegate {

	public static void invokeBeanFactoryPostProcessors(
			ConfigurableListableBeanFactory beanFactory, List<BeanFactoryPostProcessor> beanFactoryPostProcessors) {

		// Invoke BeanDefinitionRegistryPostProcessors first, if any.
		// 记录spring 容器内部执行过的后置处理器，用于去重，避免重复执行后置处理动作
		Set<String> processedBeans = new HashSet<>();

		if (beanFactory instanceof BeanDefinitionRegistry) {
			// 已知 ApplicationContext 的 beanFactory 类型为: DefaultListableBeanFactory, 其实现了接口 BeanDefinitionRegistry

			// 转型为接口类型
			BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;

			// 记录处理 "BeanFactory"的 后置处理器
			List<BeanFactoryPostProcessor> regularPostProcessors = new ArrayList<>();

			// 记录 BeanDefinition注册动作相关的，后置处理器，可以认为它是总集，
			// 不仅记录用户注入的后置处理器，也用来记录spring 自己注入的后置处理器
			List<BeanDefinitionRegistryPostProcessor> registryProcessors = new ArrayList<>();

			// 处理硬编码注册的后处理器, 所谓硬编码可以认为是用户自己通过硬编码注入的，后置处理器，而非 spring 容器自己内置的后置处理器
			// 通过 AbstractApplicationContext.addBeanFactoryPostProcessor() 方法添加
			for (BeanFactoryPostProcessor postProcessor : beanFactoryPostProcessors) {
				// 遍历所有硬编码的后置处理器，并执行其中关于 BeanDefinition 的后置处理动作
				if (postProcessor instanceof BeanDefinitionRegistryPostProcessor) {
					// 对于 BeanDefinitionRegistryPostProcessor 类型的后处理器
					// [ 执行后置处理器实现的方法，并记录 ]
					BeanDefinitionRegistryPostProcessor registryProcessor =
							(BeanDefinitionRegistryPostProcessor) postProcessor;
					registryProcessor.postProcessBeanDefinitionRegistry(registry);  // 执行
					registryProcessors.add(registryProcessor);  // 记录
				} else {
					// 记录常规的 beanFactoryPostProcessor  [ 只记录但是不执行 ]
					regularPostProcessors.add(postProcessor);
				}
			}

			// Do not initialize FactoryBeans here: We need to leave all regular beans
			// uninitialized to let the bean factory post-processors apply to them!
			// Separate between BeanDefinitionRegistryPostProcessors that implement
			// PriorityOrdered, Ordered, and the rest.
			List<BeanDefinitionRegistryPostProcessor> currentRegistryProcessors = new ArrayList<>();

			// First, invoke the BeanDefinitionRegistryPostProcessors that implement PriorityOrdered.
			// 根据类型 BeanDefinitionRegistryPostProcessor 获取工厂中的所有该类型的后处理器的 beanName
			String[] postProcessorNames =
					beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
			for (String ppName : postProcessorNames) {
				// 只处理实现了 PriorityOrdered 接口的后置处理器
				if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
					currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
					processedBeans.add(ppName); // 记录处理过的，避免重复处理
				}
			}
			sortPostProcessors(currentRegistryProcessors, beanFactory);  // 对实现了排序接口的全部后置处理器排序
			registryProcessors.addAll(currentRegistryProcessors); // 添加到总集中
			// 激活所有  currentRegistryProcessors 中的后处理器, 很简单，就是对 registry 应用这下后置处理器定义的行为
			invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);
			currentRegistryProcessors.clear();  // 清理复用

			// Next, invoke the BeanDefinitionRegistryPostProcessors that implement Ordered.
			// 根据类型 BeanDefinitionRegistryPostProcessor 获取工厂中的所有该类型的后处理器
			// 【 有没有发现，有趣的是这里获取后置处理器的动作跟上边一模一样，那么说明前边的后置处理器应用后，可能导致 容器中的 后置处理器数量变更, 所以干脆重新获取一次 】
			postProcessorNames = beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
			for (String ppName : postProcessorNames) {
				// 仅处理未处理过的 && 且实现了Ordered接口的，后置处理器所以上边重复获取 postProcessorNames 的行为就可以解释了，因为这里做了去重
				if (!processedBeans.contains(ppName) && beanFactory.isTypeMatch(ppName, Ordered.class)) {
					currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
					processedBeans.add(ppName); // 记录已经处理过, 用作去重
				}
			}

			// 如下是老三样了
			sortPostProcessors(currentRegistryProcessors, beanFactory); // 排序
			registryProcessors.addAll(currentRegistryProcessors); // 记录到总集
			invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry); // 应用后置动作
			currentRegistryProcessors.clear(); // 清理服用

			// Finally, invoke all other BeanDefinitionRegistryPostProcessors until no further ones appear. 
			// 激活剩余的 <未实现排序接口的>  所有的后处理器, 直到没有新的后处理器被发现为止
			boolean reiterate = true; // 仔细观察这个变量的变化，一旦有任意一个后置处理器被应用，都需要重新从 spring 容器获取，可能新增的后置处理器。所以才会有结论：这个循环会执行到，直到没有新的后处理器被发现为止
			while (reiterate) {
				reiterate = false;
				postProcessorNames = beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
				for (String ppName : postProcessorNames) {
					// 处理未被 processedBeans 记录的其它，未实现过排序接口的后置处理器
					if (!processedBeans.contains(ppName)) {
						currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
						processedBeans.add(ppName);
						reiterate = true;
					}
				}
				// 老三样
				sortPostProcessors(currentRegistryProcessors, beanFactory);
				registryProcessors.addAll(currentRegistryProcessors);
				invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);
				currentRegistryProcessors.clear();
			}

			// Now, invoke the postProcessBeanFactory callback of all processors handled so far.
			// BeanDefinitionRegistryPostProcessor 是负责对 bean元数据[ BeanDefinition ]进行后置处理。
			// [我们在学习 ApplicationContext 的流程中，已经介绍过，它已经继承了 BeanFactory( DefaultListableBeanFactory ) 的全部能力]

			// 通过前边重复执行的老三样，我们已经完成了全部的 BeanDefinition 的后置处理  <可以认为是增强、校验等动作>
			// 然后呢 BeanDefinitionRegistryPostProcessor 继承自 BeanFactoryPostProcessor
			// 所以，接下来要给 beanFactory 应用所有的后置处理器，并 [增强、校验] beanFactory
			invokeBeanFactoryPostProcessors(registryProcessors, beanFactory);
			invokeBeanFactoryPostProcessors(regularPostProcessors, beanFactory);
		} else {
			// 此分支表明，若 beanFactory 没有实现 BeanDefinitionRegistry 接口
			// 只需要处理 BeanFactoryPostProcessor 类型的后置处理器即可
			invokeBeanFactoryPostProcessors(beanFactoryPostProcessors, beanFactory);
		}

		// Do not initialize FactoryBeans here: We need to leave all regular beans
		// uninitialized to let the bean factory post-processors apply to them!
		// 根据类型：BeanFactoryPostProcessor 从工厂中获取所有[后置处理器]。
		// 是不是又有 BeanDefinitionRegistryPostProcessor 老三样的味道了?
		// 前边的环节中已经执行过一些 BeanFactoryPostProcessor, 这可能导致容器中 BeanFactoryPostProcessor 类型的后置处理器增多
		// 注意这里是从容器中获取了全部该类型的后置处理器，可以留心后续环节，必定有反制的去重手段
		String[] postProcessorNames = beanFactory.getBeanNamesForType(BeanFactoryPostProcessor.class, true, false);

		// 根据是否实现过排序接口，实现了何种排序接口进行区分并记录: BeanFactoryPostProcessor 类型后置处理器
		List<BeanFactoryPostProcessor> priorityOrderedPostProcessors = new ArrayList<>();
		List<String> orderedPostProcessorNames = new ArrayList<>();
		List<String> nonOrderedPostProcessorNames = new ArrayList<>();
		for (String ppName : postProcessorNames) {
			if (processedBeans.contains(ppName)) {
				// 忽略已经执行过的
				// skip - already processed in first phase above
			} else if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
				priorityOrderedPostProcessors.add(beanFactory.getBean(ppName, BeanFactoryPostProcessor.class));
			} else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
				orderedPostProcessorNames.add(ppName);
			} else { //  未实现任何排序接口
				nonOrderedPostProcessorNames.add(ppName);
			}
		}

		// 如下全是老三样了，排序并应用。它们都 需要 通过 beanFactory.getBean(name) 从容器中拿到，对应的后置区里器对象；
		// 我们前边讨论的都只是这些 后置处理器的 beanName 而已
		// First, invoke the BeanFactoryPostProcessors that implement PriorityOrdered.
		sortPostProcessors(priorityOrderedPostProcessors, beanFactory);
		invokeBeanFactoryPostProcessors(priorityOrderedPostProcessors, beanFactory);

		// Next, invoke the BeanFactoryPostProcessors that implement Ordered.
		List<BeanFactoryPostProcessor> orderedPostProcessors = new ArrayList<>();
		for (String postProcessorName : orderedPostProcessorNames) {
			orderedPostProcessors.add(beanFactory.getBean(postProcessorName, BeanFactoryPostProcessor.class));
		}
		sortPostProcessors(orderedPostProcessors, beanFactory);
		invokeBeanFactoryPostProcessors(orderedPostProcessors, beanFactory);  //  实现了 Ordered 接口 

		// Finally, invoke all other BeanFactoryPostProcessors.
		List<BeanFactoryPostProcessor> nonOrderedPostProcessors = new ArrayList<>();
		for (String postProcessorName : nonOrderedPostProcessorNames) {
			nonOrderedPostProcessors.add(beanFactory.getBean(postProcessorName, BeanFactoryPostProcessor.class));
		}
		invokeBeanFactoryPostProcessors(nonOrderedPostProcessors, beanFactory);

		// 到这里，所有: BeanFactoryPostProcessor 类型的后置处理器应用结束
		// 至于它为什么没有像 BeanDefinitionRegistryPostProcessor 类型的后置处理器那样，陷入死循环直至没有后置处理器新增?
		// 我想的是通过 beanFactory.getBeanNamesForType(BeanFactoryPostProcessor.class, ...) 方法
		// 获取到的该类型的后置处理器, 都是 spring 官方自己内置进去的，他们很明确，这些被内置进去的后置处理器,
		// 不会再触发别的 BeanFactoryPostProcessor 类型的后置处理器被注入容器。

		// 话匣子打开了这里再拓展一下, 你可能在想: 你自己开发一连串的实现了 BeanFactoryPostProcessor 接口的后置处理器
		// 并通过链式依赖，互相把对方注入 beanFactory 中，那么这里不进入死循环判断，那么是不是有可能造成这种链式注入的，后置处理器不会被全部执行呢？

		// 好吧我也在想这个问题, spring 自己官方注入进去的后置处理器, spring 他自己能保证，
		// 那么我们只能保证自己在工作中别整这种花活，刻意钻它的空子, 毕竟这对大家都没啥好处，
		// 而且这属于对 spring 框架的拓展行为了，没有点功底的人应该不太会这么干，而有功底的人也不会犯这种错误吧？

		// 我姑且把它算成一个缺陷, 如果有我遗漏了的细节，欢迎指正。

		// Clear cached merged bean definitions since the post-processors might have
		// modified the original metadata, e.g. replacing placeholders in values...
		beanFactory.clearMetadataCache();
	}

	// 注册后处理器  
	public static void registerBeanPostProcessors(
			ConfigurableListableBeanFactory beanFactory, AbstractApplicationContext applicationContext) {

		// 从spring 容器中获取所有 BeanPostProcessor 类型的后置处理器
		String[] postProcessorNames = beanFactory.getBeanNamesForType(BeanPostProcessor.class, true, false);

		// Register BeanPostProcessorChecker that logs an info message when
		// a bean is created during BeanPostProcessor instantiation, i.e. when
		// a bean is not eligible for getting processed by all BeanPostProcessors.
		int beanProcessorTargetCount = beanFactory.getBeanPostProcessorCount() + 1 + postProcessorNames.length;
		// 或许你会好奇上一行为什么 + 1 ; 因为代表的是 BeanPostProcessorChecker
		beanFactory.addBeanPostProcessor(new BeanPostProcessorChecker(beanFactory, beanProcessorTargetCount));
		// getBean 执行时，如果有某个bean，没有被任意一个 BeanPostprocessor 处理，那么它会被 BeanPostProcessorChecker 记录到日志

		// Separate between BeanPostProcessors that implement PriorityOrdered,
		// Ordered, and the rest.
		// 这里也是老三样了, 根据类型分开记录
		List<BeanPostProcessor> priorityOrderedPostProcessors = new ArrayList<>(); // 直接记录bean 本体
		List<BeanPostProcessor> internalPostProcessors = new ArrayList<>(); // 直接记录bean 本体
		List<String> orderedPostProcessorNames = new ArrayList<>(); // 只记录 后置处理器的 beanName
		List<String> nonOrderedPostProcessorNames = new ArrayList<>(); // 只记录 后置处理器的 beanName
		// 判断是否实现了排序接口 
		for (String ppName : postProcessorNames) {
			if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
				BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
				priorityOrderedPostProcessors.add(pp);
				if (pp instanceof MergedBeanDefinitionPostProcessor) {
					internalPostProcessors.add(pp);
				}
			} else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
				orderedPostProcessorNames.add(ppName);
			} else {
				nonOrderedPostProcessorNames.add(ppName);  // 记录, 未实现排序接口的, 后置处理器-bean名称
			}
		}

		// 1.注册 实现了 PriorityOrdered 接口的 后处理器
		// First, register the BeanPostProcessors that implement PriorityOrdered.
		sortPostProcessors(priorityOrderedPostProcessors, beanFactory); // 按照 PriorityOrdered 接口排序
		// 可以进入这个方法一探究竟，它真的只是把 BeanPostProcessor 注册给了 BeanFactory
		registerBeanPostProcessors(beanFactory, priorityOrderedPostProcessors);

		// Next, register the BeanPostProcessors that implement Ordered.
		// 根据beanName 从容器中，把后置处理器的本体 bean 捞上来
		List<BeanPostProcessor> orderedPostProcessors = new ArrayList<>();
		for (String ppName : orderedPostProcessorNames) {
			BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
			orderedPostProcessors.add(pp);
			if (pp instanceof MergedBeanDefinitionPostProcessor) {
				internalPostProcessors.add(pp);
			}
		}
		// 2.注册 实现了 Ordered 接口的后处理器
		sortPostProcessors(orderedPostProcessors, beanFactory); // 排序
		registerBeanPostProcessors(beanFactory, orderedPostProcessors); // 注册捞上来的 后置处理器本体

		// Now, register all regular BeanPostProcessors.
		// 还是根据beanName 从容器捞 后置处理器的本体
		List<BeanPostProcessor> nonOrderedPostProcessors = new ArrayList<>();
		for (String ppName : nonOrderedPostProcessorNames) {
			// 根据记录的名称获取  未实现排序接口的 后处理器的  bean  （可能存在后处理器还未被注册就已经开始了 bean的初始化的情况）
			// 处理的目的时保证  后处理器都有实际 对应的 bean  ??? 
			BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
			nonOrderedPostProcessors.add(pp);
			if (pp instanceof MergedBeanDefinitionPostProcessor) {
				internalPostProcessors.add(pp);
			}
		}
		// 3. 注册没有实现排序接口的 BeanPostProcessor 后置处理器
		registerBeanPostProcessors(beanFactory, nonOrderedPostProcessors);

		// Finally, re-register all internal BeanPostProcessors.
		// 4.注册 MergedBeanDefinitionPostProcessor 类型的后处理器
		// 从前文也能看出来在, 每次根据beanName从容器中把后置处理 bean 捞上来后，都会判断类型
		// 不难发现 他们的接口存在父子关系，那么就是说同一个后置处理器 bean 可能肩负着不同的后置处理动作:
		// 		- BeanPostprocessor: 创建bean时，在初始化前和初始化之后应用
		// 		- MergedBeanDefinitionPostProcessor: 如果看过前边的 getBean() 系列文章，想必对它也不会陌生, 它也是 bean
		// 			创建过程中的一环，它作用于对指定 BeanDefinition 的解析动作结束之后
		sortPostProcessors(internalPostProcessors, beanFactory); // 排序
		registerBeanPostProcessors(beanFactory, internalPostProcessors); // 注册

		// 1 看到这里，截至对 internalPostProcessors 记录的后置处理器的注册行为
		// 2 再回过头去看上边对它的处理，你会发现 internalPostProcessors 和另外三个容器所记录的后置处理器是有交叉的，
		// 		而且注册 internalPostProcessors所用的方法并没有什么不同, 就是说这四个容器所记录的全部 后置处理器，都被注册到了同样的地方
		// 3 你可能在想，已经被作为 BeanPostProcessor 类型注册过了，那么这里再去注册一遍，是否会造成重复呢？
		// 4 实施上不会, 你可以去看 beanFactory中实现的 addBeanPostProcessor() 方法，它每接收到一个注册请求时，
		// 		默认执行的都是先 remove() 一遍后, 才去执行的 add() 这样就确保了不会重复
		// 5 至于为什么要多此一举，那么可能是因为我们暂未得知的原因, BeanPostProcessor 必须先于
		// 		MergedBeanDefinitionPostProcessor 被注入到 beanFactory中？


		// Re-register post-processor for detecting inner beans as ApplicationListeners,
		// moving it to the end of the processor chain (for picking up proxies etc).
		// 处理监听事件
		// 实际上在 AbstractApplicationContext.prepareBeanFactory() 我们已经见识过它了
		// 它只负责对事件监听类的 bean 做后置处理
		beanFactory.addBeanPostProcessor(new ApplicationListenerDetector(applicationContext));
	}

	private static void sortPostProcessors(List<?> postProcessors, ConfigurableListableBeanFactory beanFactory) {
		Comparator<Object> comparatorToUse = null;
		if (beanFactory instanceof DefaultListableBeanFactory) {
			comparatorToUse = ((DefaultListableBeanFactory) beanFactory).getDependencyComparator();
		}
		if (comparatorToUse == null) {
			comparatorToUse = OrderComparator.INSTANCE;
		}
		postProcessors.sort(comparatorToUse);
	}

	/**
	 * Invoke the given BeanDefinitionRegistryPostProcessor beans.
	 */
	private static void invokeBeanDefinitionRegistryPostProcessors(
			Collection<? extends BeanDefinitionRegistryPostProcessor> postProcessors, BeanDefinitionRegistry registry) {

		for (BeanDefinitionRegistryPostProcessor postProcessor : postProcessors) {
			postProcessor.postProcessBeanDefinitionRegistry(registry);
		}
	}

	/**
	 * Invoke the given BeanFactoryPostProcessor beans.
	 */
	private static void invokeBeanFactoryPostProcessors(
			Collection<? extends BeanFactoryPostProcessor> postProcessors, ConfigurableListableBeanFactory beanFactory) {

		for (BeanFactoryPostProcessor postProcessor : postProcessors) {
			postProcessor.postProcessBeanFactory(beanFactory);
		}
	}

	/**
	 * Register the given BeanPostProcessor beans.
	 */
	private static void registerBeanPostProcessors(
			ConfigurableListableBeanFactory beanFactory, List<BeanPostProcessor> postProcessors) {

		for (BeanPostProcessor postProcessor : postProcessors) {
			beanFactory.addBeanPostProcessor(postProcessor);
		}
	}


	/**
	 * BeanPostProcessor that logs an info message when a bean is created during
	 * BeanPostProcessor instantiation, i.e. when a bean is not eligible for
	 * getting processed by all BeanPostProcessors.
	 */
	private static final class BeanPostProcessorChecker implements BeanPostProcessor {

		private static final Log logger = LogFactory.getLog(BeanPostProcessorChecker.class);

		private final ConfigurableListableBeanFactory beanFactory;

		private final int beanPostProcessorTargetCount;

		public BeanPostProcessorChecker(ConfigurableListableBeanFactory beanFactory, int beanPostProcessorTargetCount) {
			this.beanFactory = beanFactory;
			this.beanPostProcessorTargetCount = beanPostProcessorTargetCount;
		}

		@Override
		public Object postProcessBeforeInitialization(Object bean, String beanName) {
			return bean;
		}

		@Override
		public Object postProcessAfterInitialization(Object bean, String beanName) {
			if (!(bean instanceof BeanPostProcessor) && !isInfrastructureBean(beanName) &&
					this.beanFactory.getBeanPostProcessorCount() < this.beanPostProcessorTargetCount) {
				if (logger.isInfoEnabled()) {
					logger.info("Bean '" + beanName + "' of type [" + bean.getClass().getName() +
							"] is not eligible for getting processed by all BeanPostProcessors " +
							"(for example: not eligible for auto-proxying)");
				}
			}
			return bean;
		}

		private boolean isInfrastructureBean(@Nullable String beanName) {
			if (beanName != null && this.beanFactory.containsBeanDefinition(beanName)) {
				BeanDefinition bd = this.beanFactory.getBeanDefinition(beanName);
				return (bd.getRole() == RootBeanDefinition.ROLE_INFRASTRUCTURE);
			}
			return false;
		}
	}

}
