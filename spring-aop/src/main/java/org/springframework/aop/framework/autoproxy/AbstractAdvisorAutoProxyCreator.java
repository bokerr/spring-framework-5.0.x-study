/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.aop.framework.autoproxy;

import java.util.List;

import org.springframework.aop.Advisor;
import org.springframework.aop.TargetSource;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

/**
 * Generic auto proxy creator that builds AOP proxies for specific beans
 * based on detected Advisors for each bean.
 *
 * <p>Subclasses may override the {@link #findCandidateAdvisors()} method to
 * return a custom list of Advisors applying to any object. Subclasses can
 * also override the inherited {@link #shouldSkip} method to exclude certain
 * objects from auto-proxying.
 *
 * <p>Advisors or advices requiring ordering should implement the
 * {@link org.springframework.core.Ordered} interface. This class sorts
 * Advisors by Ordered order value. Advisors that don't implement the
 * Ordered interface will be considered as unordered; they will appear
 * at the end of the advisor chain in undefined order.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @see #findCandidateAdvisors
 */
@SuppressWarnings("serial")
public abstract class AbstractAdvisorAutoProxyCreator extends AbstractAutoProxyCreator {

	@Nullable
	private BeanFactoryAdvisorRetrievalHelper advisorRetrievalHelper;


	@Override
	public void setBeanFactory(BeanFactory beanFactory) {
		super.setBeanFactory(beanFactory);
		if (!(beanFactory instanceof ConfigurableListableBeanFactory)) {
			throw new IllegalArgumentException(
					"AdvisorAutoProxyCreator requires a ConfigurableListableBeanFactory: " + beanFactory);
		}
		initBeanFactory((ConfigurableListableBeanFactory) beanFactory);
	}

	protected void initBeanFactory(ConfigurableListableBeanFactory beanFactory) {
		this.advisorRetrievalHelper = new BeanFactoryAdvisorRetrievalHelperAdapter(beanFactory);
	}


	@Override
	@Nullable
	protected Object[] getAdvicesAndAdvisorsForBean(
			Class<?> beanClass, String beanName, @Nullable TargetSource targetSource) {
		// 根据 Class 以及 beanName 获取适用于当前bean的增强
		List<Advisor> advisors = findEligibleAdvisors(beanClass, beanName);
		if (advisors.isEmpty()) {
			return DO_NOT_PROXY;
		}
		return advisors.toArray();
	}

	/**
	 * Find all eligible Advisors for auto-proxying this class.
	 *
	 * @param beanClass the clazz to find advisors for
	 * @param beanName  the name of the currently proxied bean
	 * @return the empty List, not {@code null},
	 * if there are no pointcuts or interceptors
	 * @see #findCandidateAdvisors
	 * @see #sortAdvisors
	 * @see #extendAdvisors
	 */
	protected List<Advisor> findEligibleAdvisors(Class<?> beanClass, String beanName) {
		// ctrl + shift + <left>  同样也能发现 findCandidateAdvisors() 方法的多个实现
		// 但是这两个实现了 findCandidateAdvisors() 方法的是父子关系
		// 	并且子类中的重写也非全部覆盖：
		// 		- AbstractAdvisorAutoProxyCreator.findCandidateAdvisors(), 处理的是继承自类Advisor.class的AOP增强
		//  	- AnnotationAwareAspectJAutoProxyCreator.findCandidateAdvisors() 处理的是声明了 @Aspect 注解的bean,
		//  		并且需要忽略 @AspectJ 注解声明, 因为它并不适用于 Spring AOP
		List<Advisor> candidateAdvisors = findCandidateAdvisors();// [? extends Advisor] + [@Aspect]

		// 筛选适用于，当前指定的普通Bean， 的AOP增强
		List<Advisor> eligibleAdvisors = findAdvisorsThatCanApply(candidateAdvisors, beanClass, beanName);

		// 截至目前我们说的都是  @Aspect修饰的AOP 或者 extends Advisor 类型
		// 实际上还包含另一类AOP： @AspectJ 但是我们之前看 @Aspect 解析时就提到了，@AspectJ 并不适用用spring AOP

		// 如下代码做的事情是，判断所有的 Advisor 中，是否有 AspectJ 类型的，如果有向其中添加一个默认的：
		// 		- 类型为 ExposeInvocationInterceptor 的 Advisor
		//      - 添加在 index = 0的位置，说明它需要被最先应用
		extendAdvisors(eligibleAdvisors);
		if (!eligibleAdvisors.isEmpty()) {
			// 对所有的 Advisor 排序, 虽然 Advisor 空空如也，但是用户自己拓展的时候保不齐就实现了 Order 接口呢？
			eligibleAdvisors = sortAdvisors(eligibleAdvisors);
			// ctrl + shift + left 能够看到它有两个实现, 可以自行观察其中的异同
			// 实际上在这种具备多个实现的情形下，我们应该根据入口类的类图来决定，越接近入口的，才是我们应该优先看的
		}
		// 返回有效的，可以被应用到bean上的 AOP 增强 <且已经排序完成: 拦截连执行顺序>
		return eligibleAdvisors;
	}

	/**
	 * Find all candidate Advisors to use in auto-proxying.
	 *
	 * @return the List of candidate Advisors
	 */
	protected List<Advisor> findCandidateAdvisors() {
		// 再看这个断言，注入的 AOP 处理工具不能为空, 我只能告诉你去看类图:  BeanFactoryAware.setBeanFactory() 记得
		// 当然你也不会忘记，AOP 发生的时机是after init， 所以装配器 BeanFactoryAware 必定已经被应用，所以这里的断言必定为真
		Assert.state(this.advisorRetrievalHelper != null, "No BeanFactoryAdvisorRetrievalHelper available");
		return this.advisorRetrievalHelper.findAdvisorBeans();
	}

	/**
	 * Search the given candidate Advisors to find all Advisors that
	 * can apply to the specified bean.
	 *
	 * @param candidateAdvisors the candidate Advisors·
	 * @param beanClass         the target's bean class
	 * @param beanName          the target's bean name
	 * @return the List of applicable Advisors
	 * @see ProxyCreationContext#getCurrentProxiedBeanName()
	 */
	protected List<Advisor> findAdvisorsThatCanApply(
			List<Advisor> candidateAdvisors, Class<?> beanClass, String beanName) {

		ProxyCreationContext.setCurrentProxiedBeanName(beanName);  // 缓存 
		try {
			// candidateAdvisors: Spring 容器中所有合法的 AOP 增强操作
			// beanClass: 当前指定加载的bean类Class 对象
			//  	==>    根据class 可以反射得到方法，然后就可以根据方法信息、类模块信息，去对全局的AOP操作进行过滤了

			// 根据 PointCut 表达式过滤
			return AopUtils.findAdvisorsThatCanApply(candidateAdvisors, beanClass);
		} finally {
			// 清理缓存
			ProxyCreationContext.setCurrentProxiedBeanName(null);
		}
	}

	/**
	 * Return whether the Advisor bean with the given name is eligible
	 * for proxying in the first place.
	 *
	 * @param beanName the name of the Advisor bean
	 * @return whether the bean is eligible
	 */
	protected boolean isEligibleAdvisorBean(String beanName) {
		return true;
	}

	/**
	 * Sort advisors based on ordering. Subclasses may choose to override this
	 * method to customize the sorting strategy.
	 *
	 * @param advisors the source List of Advisors
	 * @return the sorted List of Advisors
	 * @see org.springframework.core.Ordered
	 * @see org.springframework.core.annotation.Order
	 * @see org.springframework.core.annotation.AnnotationAwareOrderComparator
	 */
	protected List<Advisor> sortAdvisors(List<Advisor> advisors) {
		// 默认排序行为： Spring容器的@Order注解, JDK 原生的 @Priority ...  根据这些注解的配置值进行排序

		// 比较接口的实现类： AnnotationAwareOrderComparator
		AnnotationAwareOrderComparator.sort(advisors);
		// 更多排序详情，请参阅:  OrderComparator.doCompare()
		return advisors;
	}

	/**
	 * Extension hook that subclasses can override to register additional Advisors,
	 * given the sorted Advisors obtained to date.
	 * <p>The default implementation is empty.
	 * <p>Typically used to add Advisors that expose contextual information
	 * required by some of the later advisors.
	 *
	 * @param candidateAdvisors the Advisors that have already been identified as
	 *                          applying to a given bean
	 */
	protected void extendAdvisors(List<Advisor> candidateAdvisors) {
	}

	/**
	 * This auto-proxy creator always returns pre-filtered Advisors.
	 */
	@Override
	protected boolean advisorsPreFiltered() {
		return true;
	}


	/**
	 * Subclass of BeanFactoryAdvisorRetrievalHelper that delegates to
	 * surrounding AbstractAdvisorAutoProxyCreator facilities.
	 */
	private class BeanFactoryAdvisorRetrievalHelperAdapter extends BeanFactoryAdvisorRetrievalHelper {

		public BeanFactoryAdvisorRetrievalHelperAdapter(ConfigurableListableBeanFactory beanFactory) {
			super(beanFactory);
		}

		@Override
		protected boolean isEligibleBean(String beanName) {
			return AbstractAdvisorAutoProxyCreator.this.isEligibleAdvisorBean(beanName);
		}
	}

}
