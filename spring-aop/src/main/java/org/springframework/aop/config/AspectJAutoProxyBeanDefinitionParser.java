/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.aop.config;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.lang.Nullable;

/**
 * {@link BeanDefinitionParser} for the {@code aspectj-autoproxy} tag,
 * enabling the automatic application of @AspectJ-style aspects found in
 * the {@link org.springframework.beans.factory.BeanFactory}.
 *
 * @author Rob Harrop
 * @author Juergen Hoeller
 * @since 2.0
 */
class AspectJAutoProxyBeanDefinitionParser implements BeanDefinitionParser {

	@Override
	@Nullable
	public BeanDefinition parse(Element element, ParserContext parserContext) {
		// 根据条件注册或者升级：AspectJAnnotationAutoProxyCreator
		/**
		 * <aop:aspectj-autoproxy/>
		 */
		// 再次强调: parserContext 携带着 ( BeanFactory[BeanDefinitionRegistry] )
		AopNamespaceUtils.registerAspectJAnnotationAutoProxyCreatorIfNecessary(parserContext, element);

		// 处理注解中的子类
		/**
		 * <aop:aspectj-autoproxy>
		 * 		<aop:include name="anAspect"/>
		 * </aop:aspectj-autoproxy>
		 */
		extendBeanDefinition(element, parserContext);
		return null;
		// 写在 parse() 方法的结束:
		// - 回顾一下, 我们已经为 <aspectj-autoproxy> 标签注册了解析器，但是我们还没有看到 AOP 功能演示的拦截和增强啊
		// - 我们去看看标签的解析器 AnnotationAwareAspectJAutoProxyCreator.class 的类图:
		// BeanPostProcessor  初始化相关后置处理器
		// 		<- InstantiationAwareBeanPostProcessor 实例化-自动装配后置处理器
		// 			<- SmartInstantiationAwareBeanPostProcessor  还是实例化相关的处理器
		// 				<- AbstractAutoProxyCreator
		// 					<- AbstractAdvisorAutoProxyCreator
		// 						<- AspectJAwareAdvisorAutoProxyCreator
		// 							<- AnnotationAwareAspectJAutoProxyCreator

		// 在它的类图里我们发现了一个老伙计，后置处理器接口 * 3
		// 但是再思考一下， AOP 的原理: 它是针对具体bean方法的增强 (代理)
		// 所以 AOP 一定会在bean的: 实例化完成前后、初始化完成后，去应用增强切面的织入
		// 所以我们可以直接以 AnnotationAwareAspectJAutoProxyCreator.java 文件按为起点, 在其类图中寻找:
		// 		BeanPostProcessor 和  InstantiationAwareBeanPostProcessor 接口方法的实现
		// 		[ 后置处理器 XxxPostProcessor在BeanFactory.getBean(beanName)的流程中的,特定时间节点被应用{ 初始化前、初始化后 }]
		// 最后，在它的超类: AbstractAutoProxyCreator 中找到了，有具体方法的如下两个实现，他们都能如果条件满足都能进行 AOP 织入
		//  - InstantiationAwareBeanPostProcessor.postProcessBeforeInstantiation(Class<?> beanClass, String beanName)
		// 		它在bean实例化之前就介入，并且在完成 AOP 增强织入后，会短路后续的方法
		//  - BeanPostProcessor().postProcessAfterInitialization(@Nullable Object bean, String beanName)
		// 		它负责 AOP 增强的具体实现
		// 实际上我们在分析代码时发现， 默认情况下是不会执行 InstantiationAwareBeanPostProcessor 分支的 <可以认为缺少注入条件>
		// 所以，我们后边就将以 AbstractAutoProxyCreator.postProcessAfterInitialization() 作为起点，去分析AOP 的具体实现了
	}

	private void extendBeanDefinition(Element element, ParserContext parserContext) {
		BeanDefinition beanDef =
				parserContext.getRegistry().getBeanDefinition(AopConfigUtils.AUTO_PROXY_CREATOR_BEAN_NAME);
		if (element.hasChildNodes()) {
			addIncludePatterns(element, parserContext, beanDef);
		}
	}

	private void addIncludePatterns(Element element, ParserContext parserContext, BeanDefinition beanDef) {
		ManagedList<TypedStringValue> includePatterns = new ManagedList<>();
		NodeList childNodes = element.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node node = childNodes.item(i);
			if (node instanceof Element) {
				Element includeElement = (Element) node;
				TypedStringValue valueHolder = new TypedStringValue(includeElement.getAttribute("name"));
				valueHolder.setSource(parserContext.extractSource(includeElement));
				includePatterns.add(valueHolder);
			}
		}
		if (!includePatterns.isEmpty()) {
			includePatterns.setSource(parserContext.extractSource(element));
			beanDef.getPropertyValues().add("includePatterns", includePatterns);
		}
	}

}
